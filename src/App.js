import React from 'react';
import {getContacts} from './services/contactService';
import ListContacts from "./components/ListContacts";

class App extends React.Component{
  state = {
    contacts: getContacts()
  };
  // Delete a user
  handleDelete = (id) => {
      const contacts = this.state.contacts.filter(c => c.userId !== id.userId);
      this.setState({
          contacts
      })
  };
  render() {
    const {contacts} = this.state;
    return (
        <div>
          <ListContacts
              contacts={contacts}
              onDelete = {this.handleDelete}
          />
        </div>
    );
  }

}

export default App;
