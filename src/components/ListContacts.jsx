import React from 'react';

class ListContacts extends React.Component{
    render() {
        const {contacts, onDelete} = this.props;
        return(
            <div>
                <ol className="contact-list">
                    {contacts.map(contact => (
                        <li key={contact.userId} className="contact-list-item">
                            <div
                                className="contact-avatar"
                                style={{
                                    backgroundImage: `url(${contact.profilePicture})`
                                }}
                            />
                            <div className="contact-details">
                                <p className='username'>{`@${contact.username}`}</p>
                                <p className='text-muted'>{contact.biography}</p>
                            </div>
                            <button className="btn btn-primary btn-sm contact-remove"
                            onClick={()=>onDelete(contact)}>
                                <i className="fas fa-user-plus" />
                                Follow
                            </button>
                        </li>
                    ))}
                </ol>
            </div>
        );
    }
}

export default ListContacts;